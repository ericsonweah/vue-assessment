import {createRouter, createWebHistory} from 'vue-router'

// Routes
const routes = [
    {path: '/', component: () => import('../components/Posts.vue'), name: 'home'},
    {path: '/post/:id', component: () => import('../components/Post.vue'), params: true, name: 'post'},
]


// router object
export const router =  createRouter({
    history: createWebHistory(),
    routes
})