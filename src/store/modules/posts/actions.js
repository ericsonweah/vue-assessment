import axios from "axios"

// POST action
export const POST = async ({commit}, post = {}) => {
    if(typeof post == 'object' && JSON.stringify(post) !='{}'){
        if(post.id && Number.isInteger(post.id)){
          try{
              const postDatum = await axios.get(`https://jsonplaceholder.typicode.com/posts/${post.id}`)
              if(postDatum && postDatum.data){
                
                postDatum.data.title = post.title
                postDatum.data.body = post.body
                commit('SET_POST', postDatum.data)
              }else{
                commit('SET_POST', {})
              }
          }catch(error){
            commit('SET_POST',error.response)
          }
        }else{
            commit('SET_POST',{error: 'post id must be an integer'})
        }
    }else{
        commit('SET_POST', post)
    }
}


// POST action
export const POSTS = async ({commit}, posts = []) => {
  try{
      const getPostData = await axios.get('https://jsonplaceholder.typicode.com/posts')
      if(getPostData.data){
          if(Array.isArray(posts) && posts.length >= 1){
            commit('SET_POSTS', posts)
          }else{
            commit('SET_POSTS', getPostData.data)
          }
      }else{
        commit('SET_POSTS', [])
      }
  }catch(error){
    commit('SET_POSTS', error.response)
  }
}