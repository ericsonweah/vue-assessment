// post module mutations
export const SET_POST = (state,post = {}) => {state.post = post}
export const SET_POSTS = (state,posts = []) => {state.posts = posts}
export const SET_POST_ERROR = (state, postError = {}) => {state.postError = postError}
export const SET_POSTS_ERROR = (state, postsError = {}) => {state.postsError = postsError}