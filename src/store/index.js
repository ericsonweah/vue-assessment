import { createStore } from 'vuex'

import state from './state'
import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'

import posts from './modules/posts'
// Central (Main) Store
export const store  = createStore({
    strict: true,
    state,
    getters,
    mutations,
    actions,
    modules: {
        posts
    }

})


