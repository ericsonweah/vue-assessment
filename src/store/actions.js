// general (global) actions
export const USER = ({commit}, user = {}) => commit('SET_USER', user)
export const USERS = ({commit}, users = []) => commit('SET_USERS', users)