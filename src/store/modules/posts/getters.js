// Getters
export const post = state => state.post 
export const posts = state => state.posts
export const postError = state => state.postError
export const postsError = state => state.postsError