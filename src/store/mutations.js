// general (global) mutations
export const SET_USER = (state, user = {}) => {state.user = user}
export const SET_USERS = (state, users = []) => {state.users = users}